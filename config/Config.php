<?php
namespace config;
use core\Route;

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/30/2019
 * Time: 7:50 PM
 */
class Config
{
    const DB_HOST = '77.81.105.198';

    const DB_NAME = 'marius';

    const DB_USERNAME = 'root';

    const DB_PASSWORD = 'scoalait123';

    static public function getRoutes(){
        return [
            'homepage' => new Route('/','BlogPost','homepage'),
            'blog_post_show' => new Route('/blog-post/$id','BlogPost','show'),
            'blog_post_edit' => new Route('/blog-post/edit/$id','BlogPost','edit'),
            'blog_post_delete' => new Route('/blog-post/delete/$id','BlogPost','delete'),
        ];
    }
}