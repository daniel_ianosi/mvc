<?php
namespace core;

abstract class BaseController
{
    /** @var  Request */
    public $request;

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    

    /**
     * @param $action string
     * @param $route Route
     * @param $request
     */
    public function run($action, $route, $request)
    {
        $this->request = $request;
        return call_user_func_array(array($this, $action), $route->getParams());

    }
}