<?php
namespace core;
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 30-01-2019
 * Time: 9:25 PM
 */
class TEngine
{
    public function render($varList, $viewPath)
    {
        $absolutePath = __DIR__.'/../'.$viewPath;
       if (file_exists($absolutePath)){
            extract($varList);
            ob_start();
            include ($absolutePath);
            return ob_get_clean();

        }else{
           return '<h1 color="red"> Template error!!!</h1>';
        }
    }
}