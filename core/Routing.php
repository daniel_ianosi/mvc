<?php
namespace core;
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 2/3/2019
 * Time: 4:16 PM
 */
class Routing
{
    /**
     * @var Request
     */
    public $request;
    /**
     * @var Route[]
     */
    public $routes;

    /**
     * Routing constructor.
     * @param $request
     * @param Route[] $routes
     */
    public function __construct(Request $request, $routes)
    {
        $this->request = $request;
        $this->routes = $routes;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     * @return Routing
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param array $routes
     * @return Routing
     */
    public function setRoutes($routes)
    {
        $this->routes = $routes;
        return $this;
    }


    /**
     * @return Route
     * @throws Exception
     */
    public function getCurrentRoute()
    {

        $url = $this->getRequest()->getUrl();

        foreach($this->getRoutes() as $route) {
            $pattern = $route->getPattern();

            $matches = [];
            preg_match_all('/\$([a-zA-Z]*)/', $pattern, $matches);

            $keys = $matches[1];

            $regexPattern = str_replace('/','\/',$pattern);
            $regexPattern = preg_replace('/\$([a-zA-Z]*)/','([0-9a-zA-Z-_]*)', $regexPattern).'$';
            if(preg_match_all('/'.$regexPattern.'/', $url, $matches)) {
                unset($matches[0]);
                if (count($keys)) {
                    $values = call_user_func_array('array_merge', array_values($matches));

                    $params = array_combine($keys, $values);
                    $route->setParams($params);
                }
                return $route;
            }
        }

        throw new \Exception("No route found");

    }

}