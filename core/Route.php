<?php
namespace core;
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 1/30/2019
 * Time: 9:04 PM
 */
class Route
{
    /**
     * @var string
     */
    public $pattern;
    /**
     * @var string
     */
    public $controller;
    /**
     * @var string
     */
    public $action;

    /**
     * @var []
     */
    public $params = [];

    public function __construct($pattern, $controller, $action)
    {
        $this->pattern = $pattern;
        $this->controller = $controller;
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param string $pattern
     * @return Route
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     * @return Route
     */
    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Route
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     * @return Route
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @param $param
     * @return string
     */
    public function getParam($param) {
        if(in_array($param, $this->getParams())) {
            return $param;
        }
        else
        {
            throw new Exception("Parameter ".$param." not found!");
        }
    }

}