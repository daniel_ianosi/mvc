<?php
namespace core;

abstract class BaseComponent
{

    static public function get($action, $params)
    {
        $varList = call_user_func_array(array(static::class, $action.'Action'), $params);
        $tEngine = new \core\TEngine();
        $parts = explode('\\',static::class);
        $className = strtolower(str_replace('Component','', end($parts)));
        $viewPath = '/Views/Components/' . $className.'_' . $action. '.php';


        return $tEngine->render($varList, $viewPath);

    }
}