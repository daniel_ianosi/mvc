<?php
namespace core;
/**
 * Created by PhpStorm.
 * User: Alexandru
 * Date: 30.01.2019
 * Time: 21:08
 */
class Request
{
    /** @var array  */
    public $cookies = [];
    /**
     * @var string
     */
    public $url;
    /** @var string  */
    public $method ;
    /**
     * @var array
     */
    public $post = [];
    /** @var array */
    public $get = [];

    const POST_METHOD = 'POST';

    const GET_METHOD = 'GET';

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->get = $_GET;
        $this->post = $_POST;
        $this->url = $_SERVER['REQUEST_URI'];
        $this->cookies = $_COOKIE;
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return array
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @param $name
     * @param string $value
     * @param int $expireDays
     */
    public function setCookie($name, $value = '', $expireDays = 0) {
        setcookie($name, $value, $expireDays);
        $this->cookies[$name]=$value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getCookie($name) {
        return $this->cookies[$name];
    }

    /**
     * @param array $cookies
     * @return Request
     */
    public function setCookies($cookies)
    {
        $this->cookies = $cookies;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Request
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param array $method
     * @return Request
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

}


