<?php
namespace core;
/**
 * Created by PhpStorm.
 * User: Alexandru
 * Date: 30.01.2019
 * Time: 21:21
 */
class Response
{
    public $output = [];

    public $statusCode;

    /**
     * Response constructor.
     */
    public function __construct($output, $statusCode = 200)
    {
        $this->output = $output;
        $this->statusCode = $statusCode;
    }

    public function send() {
        http_response_code($this->statusCode);
        echo $this->output;
    }
    /**
     * @return array
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param array $output
     * @return Response
     */
    public function setOutput($output)
    {
        $this->output = $output;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return Response
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }


}