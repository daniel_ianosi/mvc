<?php
namespace Components;
use core\BaseComponent;
use core\ORM\EntityManager;
use Entities\Author;

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 2/6/2019
 * Time: 9:23 PM
 */
class SidebarComponent extends BaseComponent
{
    static public function authorsAction(){
        $em = EntityManager::getInstance();
        $authors = $em->getRepository(Author::class)->findBy([]);

        return ['authors'=>$authors];
    }
}