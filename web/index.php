
<?php

function __autoload($class)
{
    $parts = explode('\\', $class);
    require __DIR__.'\\..\\'.implode(DIRECTORY_SEPARATOR, $parts) . '.php';
}
try {
    $request = new \core\Request();
    $routing = new \core\Routing($request, \config\Config::getRoutes());

    $route = $routing->getCurrentRoute();
    $controllerName = 'Controllers\\' . $route->getController() . 'Controller';
    $controllerAction = $route->getAction() . 'Action';

    /** @var \core\BaseController $controller */
    $controller = new $controllerName();
    $varList = $controller->run($controllerAction, $route, $request);
    $tEngine = new \core\TEngine();
    $viewPath = '/Views/' . $route->getController() . '/' . $route->getAction() . '.php';

    $output = $tEngine->render($varList, $viewPath);
} catch (\Exception $ex){
    $tEngine = new \core\TEngine();
    $viewPath = '/Views/error.php';
    $output = $tEngine->render(['exception'=>$ex], $viewPath);
}
$response = new \core\Response($output);
$response->send();
