<?php include __DIR__."/../header.php" ?>

<?php foreach ($blogPosts as $blogPost): ?>
<div>
    <h1><?php echo 'Title: '.$blogPost->getTitle() ?></h1>
    <p><?php echo $blogPost->getContent() ?></p>
    <p><i><?php echo 'Author:'.$blogPost->getAuthor()->getName(); ?></i></p>
</div>
<?php endforeach; ?>
<?php include __DIR__."/../footer.php" ?>
