<?php
namespace Entities;

use core\ORM\BaseEntity;

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/21/2019
 * Time: 8:05 PM
 */
class BlogPost extends BaseEntity
{
    /** @var  string */
    public $title;

    /** @var  string */
    public $content;

    /** @var  int */
    public $authorId;

    /** @var  int */
    public $categoryId;

    /** @var  String */
    public $date;


    /** @var  Author */
    public $author;

    /** @var  Category */
    public $category;




    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return BlogPost
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return BlogPost
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return Author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param Author $author
     * @return BlogPost
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return BlogPost
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     * @return BlogPost
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return String
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param String $date
     * @return BlogPost
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }



    /**
     * @return []
     */
    public function getRelations()
    {
        return [
            'author' => [
                'type' => 'ManyToOne',
                'entity' => 'Entities\\Author',
                'column' => 'author_id'
            ],
            'category' => [
                'type' => 'ManyToOne',
                'entity' => 'Entities\\Category',
                'column' => 'category_id'
            ]
        ];
    }
}