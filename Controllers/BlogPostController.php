<?php
namespace Controllers;

use core\BaseController;
use core\ORM\EntityManager;
use Entities\BlogPost;


/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/30/2019
 * Time: 8:08 PM
 */
class BlogPostController extends BaseController
{
    public function homepageAction(){
        $em = EntityManager::getInstance();
        $blogPosts =$em->getRepository(BlogPost::class)->findBy([]);

        return ['blogPosts'=>$blogPosts];
    }


    public function showAction($id){
        $em = EntityManager::getInstance();
        $blogPost = $em->getRepository(BlogPost::class)->find($id);
        if (is_null($blogPost)){
            throw new \Exception('Blog Post with id:'.$id.' not found.');
        }
        return ['blogPost' => $blogPost];
    }

    public function deleteAction($id){
        $em = EntityManager::getInstance();
        $blogPost = $em->getRepository(BlogPost::class)->find($id);

        $em->remove($blogPost);
        $em->flush();

        return [];
    }
}